#!/usr/local/bin/python3

import csv
from datetime import datetime

price_stat = dict()

with open('GDAX-ETH_USD.csv') as f:
    reader = csv.DictReader(f)

    for row in reader:
        month = datetime.strptime(row['Date'], '%Y-%m-%d').month
        if not month in price_stat:
            price_stat[month] = {
                'min': None,
                'max': 0,
                'volume': 0
            }
        

        price_stat[month]['min'] = min(float(row['Low']), price_stat[month]['min']) if price_stat[month]['min'] is not None else float(row['Low'])
        price_stat[month]['max'] = max(float(row['High']), price_stat[month]['max'])
        price_stat[month]['volume'] += float(row['Volume'])

months = sorted(price_stat.keys())

for month in months:
    print("Month {month}".format(month=month))
    print("\tVolume: {volumes:#.3f}\n\tPrice Average {price_average:#.3f}".format(
        volumes=price_stat[month]['volume'],
        price_average=price_stat[month]['min'] + price_stat[month]['max'] / 2
    ))
