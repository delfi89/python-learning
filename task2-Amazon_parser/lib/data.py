from bs4 import BeautifulSoup
from urllib.parse import urlparse, urljoin


class DataHandler:
    _content = ''
    _url = ''

    def __init__(self, url):
        self._url = urlparse(url)

    def set_content(self, text):
        self._content = BeautifulSoup(text, 'html.parser')

    def get_next_link(self):
        url_object = self._content.find(id="pagnNextLink")
        try:
            url_path = url_object.get('href')
            return urljoin(self._url.geturl(), url_path) if url_path != "" else False
        except AttributeError:
            return False

    def get_products(self):
        pass
