from fake_useragent import UserAgent

import requests


class HTTPParser:
    _last_url = ''

    def __init__(self, url):
        self._start_url = url

    def get_content(self, url):
        ua = UserAgent()

        headers = {'user-agent': ua.random}

        content = requests.get(url, headers=headers)
        return content.text
