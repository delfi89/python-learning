import sys

from lib import HTTPParser
from lib import DataHandler


def run_project(args):
    url = "https://www.amazon.com/s/ref=sr_pg_1?srs=7296466011"

    http = HTTPParser(url)
    data_handler = DataHandler(url)

    data_handler.set_content(http.get_content(url))

    while data_handler.get_next_link():
        pass


if __name__ == '__main__':
    run_project(sys.argv)
